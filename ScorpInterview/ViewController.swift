//
//  ViewController.swift
//  ScorpInterview
//
//  Created by fatih inan on 6.03.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noOneLbl: UILabel!
    
    var refresher: UIRefreshControl!
    
    var nexts: String?
    var maxCellIndex = 0
    
    var people = [Person]() {
        didSet {
            self.noOneLbl.isHidden = self.people.count > 0
            self.collectionView.refreshControl?.endRefreshing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = UIRefreshControl()
        collectionView.alwaysBounceVertical = true
        refresher.tintColor = UIColor.red
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView.refreshControl = refresher
        
        refreshList()
    }
    
    @objc func refreshList() {
        nexts = nil
        fetchNext(initial: true)
    }
    
    func show(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func fetchNext(initial: Bool) {
        if !initial && nexts == nil {
            show(message: "No one left!")
            return
        }
        
        DataSource.fetch(next: nexts) { (_response, _error) in
            if let error = _error {
                print(error)
                self.fetchNext(initial: initial)
            } else if let response = _response {
                self.nexts = response.next
                if initial {
                    self.people = response.people
                    self.collectionView.reloadData()
                } else {
                    let newPeople = response.people.filter { (newPerson) -> Bool in
                        !self.people.contains { (person) -> Bool in
                            newPerson.id == person.id
                        }
                    }
                    let startIndex = self.people.count
                    self.people.append(contentsOf: newPeople)
                    var indexPaths = [IndexPath]()
                    for i in startIndex..<(newPeople.count + startIndex) {
                        indexPaths.append(IndexPath(item: i, section: 0))
                    }
                    self.collectionView.insertItems(at: indexPaths)
                }
            }
        }
    }

}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        people.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCell", for: indexPath) as! PersonCell
        let person = people[indexPath.item]
        cell.person = person
                    
        if indexPath.item == (people.count - 1) {
            fetchNext(initial: false)
        }
        
        return cell
    }
    
}

