//
//  PersonCell.swift
//  ScorpInterview
//
//  Created by fatih inan on 6.03.2021.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    @IBOutlet var nameLbl: UILabel!
    
    var person: Person! {
        didSet {
            nameLbl.text = "\(person.fullName) (\(person.id))"
        }
    }
}

